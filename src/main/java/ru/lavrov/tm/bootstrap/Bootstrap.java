package ru.lavrov.tm.bootstrap;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.lavrov.tm.api.*;
import ru.lavrov.tm.command.AbstractCommand;
import ru.lavrov.tm.entity.User;
import ru.lavrov.tm.exception.command.CommandDescriptionIsInvalidException;
import ru.lavrov.tm.exception.command.CommandIsInvalidException;
import ru.lavrov.tm.exception.command.CommandNotExistsException;
import ru.lavrov.tm.exception.user.*;
import ru.lavrov.tm.exception.util.UtilAlgorithmNotExistsException;
import ru.lavrov.tm.repository.ProjectRepositoryImpl;
import ru.lavrov.tm.repository.TaskRepositoryImpl;
import ru.lavrov.tm.repository.UserRepositoryImpl;
import ru.lavrov.tm.enumerate.Role;
import ru.lavrov.tm.service.ProjectServiceImpl;
import ru.lavrov.tm.service.TaskServiceImpl;
import ru.lavrov.tm.service.UserServiceImpl;
import ru.lavrov.tm.util.HashUtil;

import java.security.NoSuchAlgorithmException;
import java.util.*;

@Getter
@NoArgsConstructor
public final class Bootstrap implements ServiceLocator{
    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepositoryImpl();
    @NotNull
    private final TaskRepository taskRepository = new TaskRepositoryImpl();
    @NotNull
    private final UserRepository userRepository = new UserRepositoryImpl();
    @NotNull
    private final ProjectService projectService = new ProjectServiceImpl(projectRepository, taskRepository, userRepository);
    @NotNull
    private final TaskService taskService = new TaskServiceImpl(taskRepository, projectRepository, userRepository);
    @NotNull
    private final UserService userService = new UserServiceImpl(userRepository);
    @NotNull
    private final Scanner input = new Scanner(System.in);
    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap();
    @Setter
    @Nullable
    private User currentUser;

    public void init(@Nullable Collection<Class> classes) throws InstantiationException, IllegalAccessException {
        initClasses(classes);
        initUsers();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = null;

        while (!"exit".equals(command)) {
            command = input.nextLine();
            try{
                execute(command);
            } catch (RuntimeException e){
                System.out.println(e.getMessage());
            }
        }
    }

    private void initClasses(@NotNull Collection<Class> classes) throws RuntimeException, IllegalAccessException, InstantiationException {
        if (classes == null || classes.isEmpty())
            return;
        for (Class command : classes) {
            if (!AbstractCommand.class.isAssignableFrom(command))
                continue;
            registry((AbstractCommand) command.newInstance());
        }
    }

    private void initUsers(){
        try {
            userService.createByLogin("user", HashUtil.getHash("user"), String.valueOf(Role.User));
            userService.createByLogin("admin", HashUtil.getHash("admin"), String.valueOf(Role.Admin));
        } catch (NoSuchAlgorithmException e) {
            System.out.println(new UtilAlgorithmNotExistsException().getMessage());
        }
    }

    private void registry(@Nullable final AbstractCommand command) throws RuntimeException {
        if (command == null)
            throw new CommandNotExistsException();
        final String cliCommand = command.getCommand();
        final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandIsInvalidException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandDescriptionIsInvalidException();
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    private void execute(@Nullable final String command) throws RuntimeException {
        if (command == null || command.isEmpty())
            throw new CommandIsInvalidException();
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null)
            throw new CommandNotExistsException();
        if (currentUser == null && !abstractCommand.isSafe())
            throw new UserIsNotAuthorizedException();
        Role role;
        if (currentUser == null)
            role = null;
        else {
            role = currentUser.getRole();
        }
        if (!hasPermission(abstractCommand.getRoles(), role))
            throw new UserDoNotHavePermissionException();
        abstractCommand.execute();
    }

    private boolean hasPermission(@Nullable final Collection<Role> listRoles, @Nullable Role role){
        if (listRoles == null) {
            return true;
        }
        Role currentUserRoleName = role;
        for (Role currentRole : listRoles) {
            if (currentRole.equals(currentUserRoleName))
                return true;
                //            if (currentRole.displayName().equals(enumerate.displayName()));
        }
        return false;
    }

    public void login(@Nullable final String login, @Nullable final String password){
        if (login == null || login.isEmpty())
            throw new UserLoginIsInvalidException();
        if (password == null || password.isEmpty())
            throw new UserPasswordIsInvalidException();
        User user = (User) userRepository.findEntityByName(login, null);
        if (user == null)
            throw new UserLoginNotExistsException();
        if (!password.equals(user.getPassword()))
            throw new UserLoginOrPasswordIsIncorrectException();
        setCurrentUser(user);
    }

    public void logout(){
        setCurrentUser(null);
    }
}
