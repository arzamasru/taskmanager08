package ru.lavrov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.lavrov.tm.api.ProjectRepository;
import ru.lavrov.tm.api.TaskRepository;
import ru.lavrov.tm.api.TaskService;
import ru.lavrov.tm.api.UserRepository;
import ru.lavrov.tm.exception.task.TaskNotExistsException;
import ru.lavrov.tm.exception.user.UserIsNotAuthorizedException;

import java.util.Collection;

public abstract class AbstractTaskService<Task> implements TaskService<Task> {
    @NotNull
    protected final ProjectRepository projectRepository;
    @NotNull
    protected final TaskRepository taskRepository;
    @NotNull
    protected final UserRepository userRepository;

    public AbstractTaskService(@Nullable final ProjectRepository projectRepository, @Nullable final TaskRepository taskRepository, @Nullable final UserRepository userRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void persist(@Nullable final Task task) throws RuntimeException {
        if (task == null)
            throw new TaskNotExistsException();
        taskRepository.persist(task);
    }

    @Override
    public void merge(@Nullable final Task task) throws RuntimeException {
        if (task == null)
            throw new TaskNotExistsException();
        taskRepository.merge(task);
    }

    @Override
    public void remove(@Nullable final String taskId, @Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            throw new UserIsNotAuthorizedException();
        if (taskId == null || taskId.isEmpty())
            throw new TaskNotExistsException();
        taskRepository.remove(taskId, userId);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null)
            throw new UserIsNotAuthorizedException();
        taskRepository.removeAll(userId);
    }

    @Override
    public Collection<Task> findAllByUser(@Nullable final String userId){
        if (userId == null)
            throw new UserIsNotAuthorizedException();
        return taskRepository.findAllByUser(userId);
    }
}
