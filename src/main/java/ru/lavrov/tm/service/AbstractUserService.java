package ru.lavrov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.lavrov.tm.api.UserRepository;
import ru.lavrov.tm.api.UserService;
import ru.lavrov.tm.exception.user.UserIsNotAuthorizedException;
import ru.lavrov.tm.exception.user.UserNotExistsException;

import java.util.Collection;

public abstract class AbstractUserService<User> implements UserService<User> {
    @NotNull protected final UserRepository userRepository;

    protected AbstractUserService(@NotNull final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void persist(@Nullable final User user) {
        if (user == null)
            throw new UserNotExistsException();
        userRepository.persist(user);
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null)
            throw new UserNotExistsException();
        userRepository.merge(user);
    }

    @Override
    public void remove(@Nullable final String entityId, @Nullable final String userId) {
        if (entityId == null || entityId.isEmpty())
            throw new UserIsNotAuthorizedException();
        userRepository.remove(entityId, userId);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            throw new UserIsNotAuthorizedException();
        userRepository.removeAll(userId);
    }

    @Override
    public Collection<User> findAllByUser(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            throw new UserIsNotAuthorizedException();
        return userRepository.findAllByUser(userId);
    }
}
