package ru.lavrov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.lavrov.tm.api.ProjectRepository;
import ru.lavrov.tm.api.ProjectService;
import ru.lavrov.tm.api.TaskRepository;
import ru.lavrov.tm.api.UserRepository;
import ru.lavrov.tm.exception.project.ProjectNotExistsException;
import ru.lavrov.tm.exception.user.UserIsNotAuthorizedException;

import java.util.Collection;

public abstract class AbstractProjectService<Project> implements ProjectService<Project> {
    @NotNull protected final ProjectRepository projectRepository;
    @NotNull protected final TaskRepository taskRepository;
    @NotNull protected final UserRepository userRepository;

    public AbstractProjectService(@NotNull final ProjectRepository projectRepository, @NotNull  final TaskRepository taskRepository, @NotNull  final UserRepository userRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void persist(@Nullable final Project project) throws RuntimeException {
        if (project == null)
            throw new ProjectNotExistsException();
        projectRepository.persist(project);
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null)
            throw new ProjectNotExistsException();
        projectRepository.persist(project);
    }

    @Override
    public void remove(@Nullable final String projectId, @Nullable final String userId) {
        if (userId == null)
            throw new UserIsNotAuthorizedException();
        if (projectId == null)
            throw new ProjectNotExistsException();
        projectRepository.remove(projectId, userId);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null)
            throw new UserIsNotAuthorizedException();
        projectRepository.removeAll(userId);
    }

    @Override
    public Collection<Project> findAllByUser(@Nullable final String userId){
        if (userId == null)
            throw new UserIsNotAuthorizedException();
        return projectRepository.findAllByUser(userId);
    }
}
