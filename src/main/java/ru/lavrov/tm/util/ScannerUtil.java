package ru.lavrov.tm.util;

import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public class ScannerUtil {
    @NotNull
    public static final Scanner INPUT = new Scanner(System.in);
}
