package ru.lavrov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.lavrov.tm.enumerate.Role;

import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public final class User {
    @Nullable
    private String id = UUID.randomUUID().toString();;
    @Nullable
    private String login;
    @Nullable
    private String password;
    @Nullable
    private Role role;

    public User(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }

    @Override
    public String toString() {
        return "login='" + login + '\'' +
                ", enumerate=" + role;
    }
}
