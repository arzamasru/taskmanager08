package ru.lavrov.tm.api;

import ru.lavrov.tm.command.AbstractCommand;
import ru.lavrov.tm.entity.User;

import java.util.Map;

public interface ServiceLocator {
    Map<String, AbstractCommand> getCommands();
    ProjectService getProjectService();
    TaskService getTaskService();
    UserService getUserService();
    User getCurrentUser();
    void login(String login, String password);
    void logout();
}
