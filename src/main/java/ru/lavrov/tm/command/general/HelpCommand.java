package ru.lavrov.tm.command.general;

import lombok.NoArgsConstructor;
import ru.lavrov.tm.command.AbstractCommand;
import ru.lavrov.tm.enumerate.Role;

import java.util.Collection;
import java.util.Map;

@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {
    private static final boolean SAFE = true;
    private static final Collection<Role> ROLES = null;
    private static final String COMMAND = "help";
    private static final String DESCRIPTION = "Show all commands.";

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        for (Map.Entry<String, AbstractCommand> entry : bootstrap.getCommands().entrySet()) {
            System.out.println(entry.getValue().getCommand() + ": " + entry.getValue().getDescription());
        }
        System.out.println();
    }

    @Override
    public boolean isSafe() {
        return SAFE;
    }

    @Override
    public Collection<Role> getRoles() {
        return ROLES;
    }
}
