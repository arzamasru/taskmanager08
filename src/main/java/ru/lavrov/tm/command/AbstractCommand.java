package ru.lavrov.tm.command;

import ru.lavrov.tm.api.ServiceLocator;
import ru.lavrov.tm.enumerate.Role;

import java.util.Collection;

public abstract class AbstractCommand {
    protected ServiceLocator bootstrap;
    public AbstractCommand(){}
    public void setBootstrap(final ServiceLocator bootstrap) {
        this.bootstrap = bootstrap;
    }
    public ServiceLocator getBootstrap() {
        return bootstrap;
    }
    public abstract String getCommand();
    public abstract String getDescription();
    public abstract boolean isSafe();
    public abstract Collection<Role> getRoles();
    public abstract void execute() throws RuntimeException;
}

