package ru.lavrov.tm.command.project;

import lombok.NoArgsConstructor;
import ru.lavrov.tm.api.ProjectService;
import ru.lavrov.tm.command.AbstractCommand;
import ru.lavrov.tm.entity.User;
import ru.lavrov.tm.exception.user.UserIsNotAuthorizedException;
import ru.lavrov.tm.enumerate.Role;
import ru.lavrov.tm.util.ScannerUtil;

import java.util.Arrays;
import java.util.Collection;

@NoArgsConstructor
public final class ProjectRemoveCommand extends AbstractCommand {
    private static final boolean SAFE = false;
    private static final Collection<Role> ROLES = Arrays.asList(Role.Admin, Role.User);
    private static final String COMMAND = "project-remove";
    private static final String DESCRIPTION = "Remove selected project.";

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws RuntimeException {
        System.out.println("[project remove]");
        System.out.println("enter name:");
        String projectName = ScannerUtil.INPUT.nextLine();
        User currentUser = bootstrap.getCurrentUser();
        if (currentUser == null)
            throw new UserIsNotAuthorizedException();
        ProjectService projectService = bootstrap.getProjectService();
        projectService.removeProjectByName(projectName, currentUser.getId());
        System.out.println("[ok]");
        System.out.println();
    }

    @Override
    public boolean isSafe() {
        return SAFE;
    }

    @Override
    public Collection<Role> getRoles() {
        return ROLES;
    }
}
