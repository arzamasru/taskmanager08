package ru.lavrov.tm.command.user;

import lombok.NoArgsConstructor;
import ru.lavrov.tm.api.UserService;
import ru.lavrov.tm.command.AbstractCommand;
import ru.lavrov.tm.entity.User;
import ru.lavrov.tm.enumerate.Role;
import ru.lavrov.tm.util.ScannerUtil;

import java.util.Collection;

@NoArgsConstructor
public final class UserPasswordUpdateCommand extends AbstractCommand {
    private static final boolean SAFE = false;
    private static final Collection<Role> ROLES = null;
    private static final String COMMAND = "user-password-change";
    private static final String DESCRIPTION = "Change user password.";

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[Change user password]");
        System.out.println("enter new password:");
        String newPassword = ScannerUtil.INPUT.nextLine();
        UserService userService = bootstrap.getUserService();
        User sessionUser = bootstrap.getCurrentUser();
        userService.updatePassword(sessionUser.getId(), newPassword);
        System.out.println("[ok]");
        System.out.println();
    }

    @Override
    public boolean isSafe() {
        return SAFE;
    }

    @Override
    public Collection<Role> getRoles() {
        return ROLES;
    }
}