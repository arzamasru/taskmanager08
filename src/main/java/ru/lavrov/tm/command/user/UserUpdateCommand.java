package ru.lavrov.tm.command.user;

import lombok.NoArgsConstructor;
import ru.lavrov.tm.api.UserService;
import ru.lavrov.tm.command.AbstractCommand;
import ru.lavrov.tm.entity.User;
import ru.lavrov.tm.exception.user.UserIsNotAuthorizedException;
import ru.lavrov.tm.enumerate.Role;
import ru.lavrov.tm.util.ScannerUtil;

import java.util.Collection;

@NoArgsConstructor
public final class UserUpdateCommand extends AbstractCommand {
    private static final boolean SAFE = false;
    private static final Collection<Role> ROLES = null;
    private static final String COMMAND = "user-update";
    private static final String DESCRIPTION = "Update user profile.";

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[Update user profile]");
        System.out.println("enter new login:");
        String newLogin = ScannerUtil.INPUT.nextLine();
        User currentUser = bootstrap.getCurrentUser();
        if (currentUser == null)
            throw new UserIsNotAuthorizedException();
        UserService userService = bootstrap.getUserService();
        userService.updateLogin(currentUser.getId(), newLogin);
        System.out.println();
    }

    @Override
    public boolean isSafe() {
        return SAFE;
    }

    @Override
    public Collection<Role> getRoles() {
        return ROLES;
    }
}