package ru.lavrov.tm.command.user;

import lombok.NoArgsConstructor;
import ru.lavrov.tm.api.UserService;
import ru.lavrov.tm.command.AbstractCommand;
import ru.lavrov.tm.exception.util.UtilAlgorithmNotExistsException;
import ru.lavrov.tm.enumerate.Role;
import ru.lavrov.tm.util.HashUtil;
import ru.lavrov.tm.util.ScannerUtil;

import java.security.NoSuchAlgorithmException;
import java.util.Collection;

@NoArgsConstructor
public final class UserRegisterCommand extends AbstractCommand {
    private static final boolean SAFE = true;
    private static final Collection<Role> ROLES = null;
    private static final String COMMAND = "register";
    private static final String DESCRIPTION = "Registration of new user.";

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[Registration]");
        System.out.println("enter login:");
        String login = ScannerUtil.INPUT.nextLine();
        System.out.println("enter password:");
        String password = ScannerUtil.INPUT.nextLine();
        System.out.println("enter your enumerate ('admin', 'user'):");
        String role = ScannerUtil.INPUT.nextLine();
        try {
            password = HashUtil.getHash(password);
        } catch (NoSuchAlgorithmException e) {
            throw new UtilAlgorithmNotExistsException();
        }
        UserService userService = bootstrap.getUserService();
        userService.createByLogin(login, password, role);
        System.out.println("[User successfully registered]");
        System.out.println();
    }

    @Override
    public boolean isSafe() {
        return SAFE;
    }

    @Override
    public Collection<Role> getRoles() {
        return ROLES;
    }
}