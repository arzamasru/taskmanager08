package ru.lavrov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.lavrov.tm.api.ProjectRepository;
import ru.lavrov.tm.entity.Project;
import ru.lavrov.tm.exception.project.ProjectExistsException;
import ru.lavrov.tm.exception.project.ProjectNameIsInvalidException;
import ru.lavrov.tm.exception.project.ProjectNotExistsException;
import ru.lavrov.tm.exception.user.UserIsNotAuthorizedException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractProjectRepository implements ProjectRepository<Project> {
    @NotNull protected final Map<String, Project> projects = new HashMap();

    @Override
    public void persist(@Nullable final Project project) throws RuntimeException{
        if (project == null)
            throw new ProjectNotExistsException();
        String id = project.getId();
        if (projects.containsKey(id))
            throw new ProjectExistsException();
        projects.put(id, project);
    }

    @Override
    public void merge(@Nullable final Project project){
        projects.put(project.getId(), project);
    }

    @Override
    public void remove(@Nullable final String projectId, @Nullable final String userId){
        if (projectId == null || projectId.isEmpty())
            throw new ProjectNotExistsException();
        if (userId == null || userId.isEmpty())
            throw new UserIsNotAuthorizedException();
        Project project = projects.get(projectId);
        if (!project.getUserId().equals(userId))
            throw new ProjectNotExistsException();
        projects.remove(projectId);
    }

    @Override
    public void removeAll(@Nullable final String userId){
        if (userId == null || userId.isEmpty())
            throw new UserIsNotAuthorizedException();
        for (Project project : findAllByUser(userId)) {
            remove(project.getId(), userId);
        }
    }

    @Override
    public Project findEntityByName(@Nullable final String projectName, @Nullable final String userId){
        if (projectName == null || projectName.isEmpty())
            throw new ProjectNameIsInvalidException();
        if (userId == null || userId.isEmpty())
            throw new UserIsNotAuthorizedException();
        Project currentProject = null;
        for (Project project: projects.values()) {
            if (projectName.equals(project.getName()) && project.getUserId().equals(userId)) {
                currentProject = project;
                break;
            }
        }
        return currentProject;
    }

    @Override
    public Collection<Project> findAllByUser(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            throw new UserIsNotAuthorizedException();
        Collection<Project> list = new ArrayList<>();
        for (Project project : projects.values()) {
            if (project.getUserId().equals(userId))
                list.add(project);
        }
        return list;
    }
}
