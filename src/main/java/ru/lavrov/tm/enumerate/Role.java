package ru.lavrov.tm.enumerate;

public enum Role {
    User("user"),
    Admin("admin");

    private String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
