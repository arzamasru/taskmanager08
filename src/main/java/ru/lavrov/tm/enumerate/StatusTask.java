package ru.lavrov.tm.enumerate;

public enum StatusTask {
    PLANNED("planned"),
    IN_PROCESS("in process"),
    DONE("done");

    private String displayName;

    StatusTask(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
